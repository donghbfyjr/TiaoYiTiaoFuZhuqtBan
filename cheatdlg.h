﻿#ifndef CHEATDLG_H
#define CHEATDLG_H

#include <QDialog>
#include <QThread>
#include <cworkthread.h>


namespace Ui {
class CheatDlg;
}

class CheatDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CheatDlg(QWidget *parent = 0);
    ~CheatDlg();

private slots:
    void on_startbtn_clicked();

    void on_stopbtn_2_clicked();

private:
    Ui::CheatDlg *ui;

    // QThread interface
protected:
    void run();

private:
    CWorkThread* m_workThread;

};

#endif // CHEATDLG_H
