- # 跳一跳辅助qt版(仅支持安卓)
- 1.手机必须ROOT，手机链接电脑后开启USB调试模式，连接后的USB选择传输文件，不要选择充电模式，手机弹窗授权确认必须点击接受。
- 2.需要使用到安卓adb，请自行下载，并设置环境变量，使adb在cmd下可用
- 
- 步骤:
- 1.手机打开调试模式，使用USB数据线连接到PC，选择传输文件模式
- 2.在PC端输入cmd,输入adb devices，能够显示已连接的设备，有可能显示未授权需要等手机弹出授权提升点确认，则可以使用本程序
- 3.打开微信跳一跳
- 4.本程序点击start